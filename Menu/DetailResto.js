import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';

import { Content, Card, CardItem, Left, Right, Icon } from 'native-base';
import axios from 'axios';

var res_id="";
export default class DetailResto extends Component{
    static navigationOptions = ({navigation})=>{
        res_id= navigation.getParam("res_id");
        return(
        title.navigation.getParam("nama_restaurant")
        )
    }

    constructor(){
        super();
        this.state={
            detailResto:[]
        }
    }

    getdetailResto(){
        axios.get(`https://developers.zomato.com/api/v2.1/restaurant?res_id=${res_id}`,{
            headers:{"user_key":"997962ec93e97cf6e3e16bd568394fdd"}
        }).
        then(res=>{
            this.setState({
                detailResto:res.data
            })
        })

    }

    componentDidMount(){
        this.getdetailResto();
    }
    render(){
        var alamat = {...this.state.detailResto.location}
        var rating = {...this.state.detailResto.user_rating}
        return(
            <View style={{flex:1}}>
                <Content>
                    <Card>
                        <CardItem>
                            <Image style={{ height: 240, width: null, flex: 1}}
                            source={{
                                uri: this.state.detailResto.featured_image
                            }}
                            />
                        </CardItem>
                        <CardItem>
                            <Left>
                                <Text>
                                    Alamat Restaurant: {alamat.address}
                                </Text>
                            </Left>
                        </CardItem>
                        <CardItem>
                            <Left>
                                <Icon name = "star" style={{backgroundColor:"#E1EE17"}}/>
                                <Text>{rating.aggregate_rating}</Text>
                            </Left>
                            <Right>
                                <Icon name="chatbubbles"/>
                                <Text>
                                   {this.state.detailResto.all_reviews_count}
                                </Text>
                            </Right>
                        </CardItem>
                    </Card>

                    <Card>
                        <CardItem>
                            <Left>
                                <Text>
                                    Jenis Masakan : {this.state.detailResto.cuisines}
                                </Text>
                            </Left>
                        </CardItem>
                    </Card>
                </Content>
            </View>
        )
    }
}