import React, { Component } from 'react';
import {Image, TouchableOpacity, View} from 'react-native';
import { Content, Card, CardItem, Text, Body } from 'native-base';
import Axios from 'axios';


export default class NamaResto extends Component{
    constructor(props){
        super(props);
        this.state={
            dataRestaurant:[],
            id_resto:this.props.id_resto
        }
    }

    getDataRestaurant(){
        Axios.get(`https://developers.zomato.com/api/v2.1/search?entity_id=${
            this.state.id_resto
        }&entity_type=city`,{
        headers:{user_key:"997962ec93e97cf6e3e16bd568394fdd"}
    }).then(res=>{
        this.setState({
            dataRestaurant:res.data.restaurants
        })
    })
    }

    componentDidMount(){
        this.getDataRestaurant();
    }
    render(){
        return(
           <Content>
               {this.state.dataRestaurant.map((data,key)=>{
                   return(
                    <View key={key}>
                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate("DetailResto", {
                        nama_restaurant: data.restaurant.name,
                        res_id: data.restaurant.R.res_id,
                        });
                    }}
                    >
                    <Card>
                    <CardItem>
                        <Body>
                        <Text>{data.restaurant.name} </Text>
                        <Text note>{this.props.nama}</Text>
                        </Body>
                    </CardItem>
                    <CardItem cardbody>
                       <Image
                       style={{height: 250, width: null, flex:1}}
                       source={{
                        uri: data.restaurant.thumb
                    }}
                    />
                    </CardItem>
                </Card>
                </TouchableOpacity>
                </View>
                   )
               })}

           </Content>
        )
    }
}