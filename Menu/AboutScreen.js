import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Image
} from 'react-native';
import Logo from './Images/logo.png'
import Icon from 'react-native-vector-icons/Ionicons';


export default class About extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
        </View>
        <Image style={styles.user} source={Logo} />
        
        <Text style={styles.profile}>ABOUT</Text>
        <Text>------------------------------------------------</Text>
        <View style={styles.textWrapper}>
          <Text style={styles.bio1}>Nama Lengkap       : Taufiq Ismail</Text>
          <Text style={styles.bio1}>Created by              : API Zomato</Text>
          <Text style={styles.bio}>Akun Media Sosial :</Text>
          <Icon style={styles.bioIcon}
            name="logo-twitter"
            color="#53B7FF"
            size={20}
          />
          <Text style={styles.akun}>@taufikikk</Text>
          <Icon style={styles.bioIcon}
            name="logo-instagram"
            color="#53B7FF"
            size={20}
          />
          <Text style={styles.akun}>@taupikan</Text>
          <Icon style={styles.bioIcon}
            name="logo-facebook"
            color="#53B7FF"
            size={20}
          />
          <Text style={styles.akun}>Taufiq Ismail</Text>
          <Text style={styles.bio}>Link Portofolio       :</Text>
          <Text style={styles.akun}>gitlab : @taufikikk</Text>
        </View>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  header: {
    marginVertical: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 25,
  },
  profile: {
    marginLeft: 50,
    marginTop: 10,
    fontSize: 26,
    fontWeight: 'bold',
  },
  user: {
    marginLeft: 130,
  },
  bioIcon: {
    marginLeft: 135,
  },
  textWrapper: {
    marginLeft: 50,
  },
  akun: {
    marginLeft: 155,
    marginTop: -20,
    marginBottom: 5,
  },
  icons: {
    marginLeft: 130,
    width: 25,
    height: 25,
  }
});
