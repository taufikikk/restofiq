import React from 'react';
import { View, Text, TextInput, StyleSheet, Button, Image } from 'react-native';
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";
// import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Logo from './Images/logo.png'

export default class LoginScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: '',
      password: '',
      isError: false,
    }
  }

  loginHandler() {
    console.log(this.state.userName, ' ', this.state.password)

    if (this.state.password === 'admin') {
      this.props.navigation.navigate('Resto', {name: this.state.userName});
    } else {
      this.setState({isError: true});
    }

  }

  render() {
    return (
      <View style={styles.container}>
        <View>
          <Image source={Logo}/>
          <Text style={styles.subTitleText}>Login</Text>
        </View>

        <View style={styles.formContainer}>
          <View style={styles.inputContainer}>

            <View>
              <Text>Username/Email</Text>
              <TextInput
                style={styles.textInput}
                placeholder='Masukkan Nama User/Email'
                onChangeText={userName => this.setState({ userName })}
              />
            </View>
          </View>

          <View style={styles.inputContainer}>

            <View>
              <Text>Password</Text>
              <TextInput
                style={styles.textInput}
                placeholder='Masukkan Password'
                onChangeText={password => this.setState({ password })}
                secureTextEntry={true}
              />
            </View>
          </View>
          <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Password Salah</Text>
          <Button title='Login' onPress={() => this.loginHandler()} />
        </View>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
 
  subTitleText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#4095C5',
    alignSelf: 'center',
    marginBottom: 16
  },
  formContainer: {
    marginTop: 20,
    justifyContent: 'center',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 16
  },
  textInput: {
    height: 32,
    width: 207,
    backgroundColor: '#F4FAFF',
  },
  errorText: {
    color: 'red',
    textAlign: 'center',
    marginBottom: 16,
  },
  hiddenErrorText: {
    color: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
  }
});
