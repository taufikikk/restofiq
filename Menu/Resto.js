import React, { Component } from 'react';
import {Container, ScrollableTab, Tab, Tabs} from 'native-base';
import NamaResto from './NamaResto'
import About from './AboutScreen'
export default class Resto extends Component {
  static navigationOptions = {header: null}
    render(){
        return(
        <Container>
        <Tabs renderTabBar={()=> <ScrollableTab />}>
          <Tab heading="Jakarta">
          <NamaResto nama="Jakarta" id_resto="74" />
          </Tab>
          <Tab heading="Bandung">
          <NamaResto nama="Bandung" id_resto="11052" />
          </Tab>
          <Tab heading="Bali">
          <NamaResto nama="Bali" id_resto="170" />
          </Tab>
          <Tab heading="About">
          <About />
          </Tab>
        </Tabs>
      </Container>
        )
    }
}